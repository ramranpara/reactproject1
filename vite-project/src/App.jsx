import React, { useState } from 'react';
import Board from './components/Board';
import './App.css'
import './styles/root.scss';

function App() {
  const [board, setBoard] = useState(Array(9).fill(null)); 
  const [isXnext, setisXnext] = useState(false); 

  
  const handleSquareClick = position => {
    if (board[position]) {
      return;
    }

   setBoard(prev => {
    return prev.map((square, pos) => {
      if (pos === position) {
        return isXnext ? 'X' : '0';
      }

      return square;
    });
   });
   
   setisXnext( (prev) => !prev );
  };


  return (
    <div className="app">
     <h1>Tic Tac To</h1>
     <Board board={board}  handleSquareClick={handleSquareClick} />
    </div>
        
  )
}

export default App
